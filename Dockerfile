FROM python:3.10
RUN apt-get update -y
RUN apt-get install -y libopenblas-dev liblapack-dev libxc-dev
RUN apt-get install -y liblapacke-dev gfortran  # req. by phono3py
RUN apt-get install -y openmpi-bin libopenmpi-dev
RUN passwd --delete root

# Info: The user setup is borrowed from the ASE dockers
RUN adduser asr --disabled-password --gecos ""  #  gecos ==> non-interactive
USER asr
WORKDIR /home/asr
ENV PATH /home/asr/.local/bin:$PATH

RUN pip install phono3py==1.22.3

RUN pip install pip --upgrade
RUN pip install matplotlib flask Click plotly
RUN pip install spglib
RUN pip install pytest

RUN pip install git+https://gitlab.com/ase/ase.git@master
RUN pip install git+https://gitlab.com/gpaw/gpaw.git@master
RUN gpaw install-data --register .

RUN pip install pymatgen
RUN pip install phonopy

RUN pip install pytest-xdist
RUN pip install pytest-mock
RUN pip install pytest-cov

# Docs depenendices.
# These could maybe go in a separate (smaller) docker
RUN pip install sphinx
RUN pip install sphinxcontrib-programoutput
RUN pip install sphinx-autodoc-typehints

# New-master-stuff
RUN pip install simplejson
