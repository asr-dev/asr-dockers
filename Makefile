build:
	docker build  -t registry.gitlab.com/asr-dev/asr:main .

build-nocache:
	docker build --no-cache -t registry.gitlab.com/asr-dev/asr:main .


shell:
	docker run --name container-main --rm -i -t registry.gitlab.com/asr-dev/asr:main bash

push:
	docker push registry.gitlab.com/asr-dev/asr:main
